import React, { Component } from 'react';
import ItemShoe from './ItemShoe';
import { connect } from 'react-redux';

class ListShoes extends Component {
  renderShoeItem = () => {
    return this.props.list.map((item, index) => {
      return <ItemShoe item={item} key={index} />;
    });
  };

  render() {
    return (
      <div className="list-shoe container">
        <div className="row g-3">{this.renderShoeItem()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoesReducer.listShoes,
  };
};

export default connect(mapStateToProps)(ListShoes);
