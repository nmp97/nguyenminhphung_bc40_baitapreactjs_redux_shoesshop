import React, { Component } from 'react';
import ListShoes from './ListShoe';
import Header from '../Header/Header';
import ModalDetailProduct from '../Modal/ModalDetailProduct/ModalDetailProduct';
import ModalCart from '../Modal/ModalCart/ModalCart';
import Footer from '../Footer/Footer';
import { ToastContainer } from 'react-toastify';

export default class ShoeShop extends Component {
  render() {
    return (
      <>
        <div className="content">
          <Header />
          <ModalCart />
          <ModalDetailProduct />
          <ListShoes />
          <Footer />
          <ToastContainer limit={3} style={{ width: 'auto' }} />
        </div>
      </>
    );
  }
}
