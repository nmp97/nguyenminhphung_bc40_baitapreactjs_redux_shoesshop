import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return <div className="display-6 fw-bold py-3 px-5 bg-body-secondary">Footer</div>;
  }
}
