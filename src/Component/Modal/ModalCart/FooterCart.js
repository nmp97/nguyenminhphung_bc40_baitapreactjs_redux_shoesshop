import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CLOSE_CART } from '../../../constants/shoesConstant';

class FooterCart extends Component {
  render() {
    return (
      <>
        <div className="d-flex justify-content-between align-content-center">
          <span>Tổng Tiền:</span>
          <span>{this.props.totalCart}$</span>
        </div>
        <div className="text-end mt-3">
          <button
            className="mx-2 btn btn-danger"
            variant="secondary"
            onClick={this.props.handleCloseModalCart}
          >
            Đóng
          </button>
          <button
            className="btn btn-primary"
            variant="primary"
            onClick={this.props.handleCloseModalCart}
          >
            Đặt hàng
          </button>
        </div>
      </>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    totalCart: state.shoesReducer.totalCart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleCloseModalCart: () => {
      let action = {
        type: CLOSE_CART,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FooterCart);
