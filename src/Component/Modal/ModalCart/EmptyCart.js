import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CLOSE_CART } from '../../../constants/shoesConstant';

class EmptyCart extends Component {
  render() {
    return (
      <div className="text-center py-5">
        <p>Chưa có sản phẩm</p>
        <button className="btn btn-outline-primary" onClick={this.props.handleCloseModalCart}>
          Tiếp tục mua sắm
        </button>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleCloseModalCart: () => {
      let action = {
        type: CLOSE_CART,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(EmptyCart);
