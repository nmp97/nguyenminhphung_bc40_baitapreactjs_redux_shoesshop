import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  increaseQuantityItemCartAction,
  decreaseQuantityItemCartAction,
  removeItemAction,
} from '../../../action/shoesAction';

class CartItem extends Component {
  renderCartItem = () => {
    return this.props.cartList.map((item) => {
      return (
        <div className="cartItem row justify-content-between align-items-center" key={item.id}>
          <div className="col-auto">
            <img src={item.image} alt="" style={{ width: 80, height: 80 }} />
          </div>
          <div className="col-auto">
            <span>{item.price * item.quantity}$</span>
          </div>
          <div className="modal-btn col-auto">
            <button>
              <span
                className="fs-5 fw-bold w-100"
                onClick={() => {
                  this.props.handleDecreaseQuantityModalCart(item.id);
                }}
              >
                -
              </span>
            </button>
            <span className="mx-2">{item.quantity}</span>
            <button>
              <span
                className="fs-5 fw-bold w-100"
                onClick={() => {
                  this.props.handleIncreaseQuantityModalCart(item.id);
                }}
              >
                +
              </span>
            </button>
          </div>
          <div className="col-auto">
            <button
              className="btn btn-outline-danger"
              onClick={() => {
                this.props.handleRemoveCartItem(item.id);
              }}
            >
              Xóa
            </button>
          </div>
        </div>
      );
    });
  };

  render() {
    return <>{this.renderCartItem()}</>;
  }
}

let mapStateToProps = (state) => {
  return {
    cartList: state.shoesReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleIncreaseQuantityModalCart: (id) => {
      dispatch(increaseQuantityItemCartAction(id));
    },

    handleDecreaseQuantityModalCart: (id) => {
      dispatch(decreaseQuantityItemCartAction(id));
    },

    handleRemoveCartItem: (id) => {
      dispatch(removeItemAction(id));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartItem);
