import React, { Component } from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/Modal';
import CartItem from './CartItem';
import EmptyCart from './EmptyCart';
import FooterCart from './FooterCart';
import { closeCartAction } from '../../../action/modalAction';
import { ModalBody } from 'react-bootstrap';

class ModalCart extends Component {
  render() {
    return (
      <div>
        <Modal show={this.props.showModalCart} onHide={this.props.handleCloseModalCart}>
          <Modal.Header closeButton>
            <Modal.Title>Giỏ Hàng Của Bạn</Modal.Title>
          </Modal.Header>
          {this.props.cartList.length === 0 ? (
            <ModalBody>
              <EmptyCart />
            </ModalBody>
          ) : (
            <>
              <Modal.Body>
                <CartItem />
              </Modal.Body>
              <Modal.Footer className="d-block">
                <FooterCart />
              </Modal.Footer>
            </>
          )}
        </Modal>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    showModalCart: state.modalReducer.showModalCart,
    cartList: state.shoesReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleCloseModalCart: () => {
      dispatch(closeCartAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalCart);
