import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import { connect } from 'react-redux';
import { closeItemAction } from '../../../action/modalAction';

class ModalDetailProduct extends Component {
  render() {
    let { name, price, description, image } = this.props.detailProduct;
    return (
      <Modal
        show={this.props.showModalDetailProduct}
        onHide={this.props.handleCloseModalDetailProduct}
      >
        <Modal.Header closeButton>
          <Modal.Title className="fs-2 fw-bold">Thông tin sản phẩm</Modal.Title>
        </Modal.Header>
        <Modal.Body className="d-flex">
          <img src={image} alt="" style={{ width: '15rem' }} />
          <div className="p-3">
            <h4>{name}</h4>
            <p className="fw-bold">Giá: {price}$</p>
            <p>
              <strong>Mô tả:</strong>
              <br />
              {description}
            </p>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={this.props.handleCloseModalDetailProduct}>
            Đóng
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    showModalDetailProduct: state.modalReducer.showModalDetailProduct,
    detailProduct: state.modalReducer.detailProduct,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleCloseModalDetailProduct: () => {
      dispatch(closeItemAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalDetailProduct);
