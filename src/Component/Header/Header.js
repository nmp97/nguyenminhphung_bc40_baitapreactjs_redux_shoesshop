import React, { Component } from 'react';
import { BagCheck } from 'react-bootstrap-icons';
import { connect } from 'react-redux';
import { showCartAction } from '../../action/modalAction';

class Header extends Component {
  render() {
    return (
      <div className="sticky-top w-100 z-3 d-flex justify-content-between align-items-center py-2 bg-danger-subtle px-5">
        <h1 className="display-6 fw-bold text-center">NMP SHOES</h1>
        <div
          className="cart d-flex align-items-center p-2 rounded position-relative"
          onClick={() => {
            this.props.handleOpenModalCart();
          }}
        >
          <BagCheck className="fs-5" />
          <span className="fs-6 lh-sm px-1">
            Giỏ
            <br />
            Hàng
          </span>
          <span className="cart-quantity">{this.props.quantityCart}</span>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    quantityCart: state.shoesReducer.quantityCart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleOpenModalCart: () => {
      dispatch(showCartAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
