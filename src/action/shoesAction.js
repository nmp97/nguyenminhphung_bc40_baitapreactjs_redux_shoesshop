import {
  ADD_TO_CART,
  INCREASE_QUANTITY,
  DECREASE_QUANTITY,
  DELETE_ITEM,
} from '../constants/shoesConstant';

export const addToCartAction = (shoe) => {
  return {
    type: ADD_TO_CART,
    payload: shoe,
  };
};

export const increaseQuantityItemCartAction = (id) => {
  return {
    type: INCREASE_QUANTITY,
    payload: id,
  };
};

export const decreaseQuantityItemCartAction = (id) => {
  return {
    type: DECREASE_QUANTITY,
    payload: id,
  };
};

export const removeItemAction = (id) => {
  return {
    type: DELETE_ITEM,
    payload: id,
  };
};
