import { CLOSE_CART, CLOSE_ITEM, SHOW_CART, SHOW_ITEM } from '../constants/shoesConstant';

export const showItemAction = (shoe) => {
  return {
    type: SHOW_ITEM,
    payload: shoe,
  };
};

export const closeItemAction = () => {
  return {
    type: CLOSE_ITEM,
  };
};

export const showCartAction = () => {
  return {
    type: SHOW_CART,
  };
};

export const closeCartAction = () => {
  return {
    type: CLOSE_CART,
  };
};
