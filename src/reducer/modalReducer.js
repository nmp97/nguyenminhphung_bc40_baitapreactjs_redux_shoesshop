import { CLOSE_CART, CLOSE_ITEM, SHOW_CART, SHOW_ITEM } from '../constants/shoesConstant';

let initialValue = {
  showModalDetailProduct: false,
  showModalCart: false,
  detailProduct: {},
};

export const modalReducer = (state = initialValue, action) => {
  switch (action.type) {
    case SHOW_ITEM: {
      return { ...state, showModalDetailProduct: true, detailProduct: action.payload };
    }

    case CLOSE_ITEM: {
      return { ...state, showModalDetailProduct: false };
    }

    case SHOW_CART: {
      return { ...state, showModalCart: true };
    }

    case CLOSE_CART: {
      return { ...state, showModalCart: false };
    }
    default:
      return state;
  }
};
