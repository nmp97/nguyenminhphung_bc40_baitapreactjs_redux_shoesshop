import {
  ADD_TO_CART,
  DECREASE_QUANTITY,
  DELETE_ITEM,
  INCREASE_QUANTITY,
} from '../constants/shoesConstant';
import { dataShoe } from '../data/dataShoe';
import { toast, Zoom } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

let initialValue = {
  listShoes: dataShoe,
  cart: [],
  quantityCart: 0,
  totalCart: 0,
};

const addToCartNotify = () => {
  toast('Thêm vào giỏ hàng thành công!', {
    type: 'success',
    position: 'top-center',
    autoClose: 1000,
    transition: Zoom,
  });
};

export const shoesReducer = (state = initialValue, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let newCart = [...state.cart];
      let newQuantityCart = state.quantityCart;
      let newTotalCart = state.totalCart;

      let index = newCart.findIndex((item) => item.id === action.payload.id);

      if (index === -1) {
        let newShoes = { ...action.payload, quantity: 1 };
        newCart.push(newShoes);
        newQuantityCart += newShoes.quantity;
        newTotalCart += newShoes.price;
      } else {
        newCart[index].quantity += 1;
        newQuantityCart += 1;
        newTotalCart += newCart[index].price;
      }

      addToCartNotify();

      return { ...state, cart: newCart, quantityCart: newQuantityCart, totalCart: newTotalCart };
    }

    case INCREASE_QUANTITY: {
      let newCart = [...state.cart];
      let newQuantityCart = state.quantityCart;
      let newTotalCart = state.totalCart;

      let index = newCart.findIndex((item) => item.id === action.payload);

      newCart[index].quantity += 1;
      newQuantityCart += 1;
      newTotalCart += newCart[index].price;

      return { ...state, cart: newCart, quantityCart: newQuantityCart, totalCart: newTotalCart };
    }

    case DECREASE_QUANTITY: {
      let newCart = [...state.cart];
      let newQuantityCart = state.quantityCart;
      let index = newCart.findIndex((item) => item.id === action.payload);
      let quantityItem = newCart[index].quantity;
      let newTotalCart = state.totalCart;

      if (quantityItem > 1) {
        newCart[index].quantity -= 1;
        newQuantityCart -= 1;
        newTotalCart -= newCart[index].price;
      } else {
        newQuantityCart -= 1;
        newTotalCart -= newCart[index].price;
        newCart.splice(index, 1);
      }

      return { ...state, cart: newCart, quantityCart: newQuantityCart, totalCart: newTotalCart };
    }

    case DELETE_ITEM: {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => item.id === action.payload);
      let newQuantityCart = state.quantityCart;
      let newTotalCart = state.totalCart;

      newQuantityCart -= newCart[index].quantity;
      newTotalCart -= newCart[index].quantity * newCart[index].price;
      newCart.splice(index, 1);

      return { ...state, cart: newCart, quantityCart: newQuantityCart, totalCart: newTotalCart };
    }

    default:
      return state;
  }
};
