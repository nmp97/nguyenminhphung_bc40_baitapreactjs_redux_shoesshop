export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_ITEM = 'DELETE_ITEM';
export const SHOW_ITEM = 'SHOW_ITEM';
export const CLOSE_ITEM = 'CLOSE_ITEM';
export const SHOW_CART = 'SHOW_CART';
export const CLOSE_CART = 'CLOSE_CART';
export const INCREASE_QUANTITY = 'INCREASE_QUANTITY';
export const DECREASE_QUANTITY = 'DECREASE_QUANTITY';
